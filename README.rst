singularity-gf
=====

Version: 0.1

This is a GeneFlow app demonstrating how to execute and pipe singularity containers.

Inputs
------

1. file: dummy input file.

Parameters
----------

1. output: name of output file.

